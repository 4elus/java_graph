public class Graph {
    private int maxN = 10;
    private int[][] mas;
    Vertex[] vertexList;
    private int curN;
    private MyStack stack = new MyStack();
    private MyQueue queue = new MyQueue();
    private char[] sortedArr;
    PriorityQ priorityQ = new PriorityQ();
    private  int curVert;

    public Graph(){
        vertexList = new Vertex[maxN];
        mas = new int[maxN][maxN];
        curN = 0;
        sortedArr = new char[maxN];
    }

    public void addVertex(char name){
        vertexList[curN++] = new Vertex(name);
    }

    public void addEdge(int start, int end, int distance){

        mas[start][end] = distance;
        mas[end][start] = distance;
    }

    public int check(int v){
        for (int i = 0; i < curN; i++) {
            if (mas[v][i] == 1 && vertexList[i].isVisited == false){
                return i;
            }
        }

        return -1;
    }

    public void passInDeep(int index){
        System.out.println(vertexList[index].name);
        vertexList[index].isVisited = true;
        stack.push(index);

        while (!stack.isEmpty()){
            int neigh = check(stack.peek());

            if (neigh == -1){
                neigh = stack.pop();
            }
            else{
                System.out.println(vertexList[neigh].name);
                vertexList[neigh].isVisited = true;
                stack.push(neigh);
            }
        }

        for (int i = 0; i < curN; i++) {
            vertexList[i].isVisited = false;
        }
    }

    public void passInWidth(int index){
        System.out.println(vertexList[index].name);
        vertexList[index].isVisited = true;
        queue.insert(index);

        int vertex;

        while (!queue.isEmpty()){
            int temp = queue.remove();

            while ((vertex = check(temp)) != -1){
                System.out.println(vertexList[vertex].name);
                vertexList[vertex].isVisited = true;
                queue.insert(vertex);
            }
        }

        for (int i = 0; i < curN; i++) {
            vertexList[i].isVisited = false;
        }
    }

    public void minPath(int index){
        vertexList[index].isVisited = true;
        stack.push(index);

        while (!stack.isEmpty()){
            int curVertex = stack.peek();
            int neigh = check(curVertex);

            if (neigh == -1){
                neigh = stack.pop();
            }
            else{
                System.out.print(vertexList[curVertex].name + "->");
                System.out.println(vertexList[neigh].name);
                vertexList[neigh].isVisited = true;
                stack.push(neigh);
            }
        }

        for (int i = 0; i < curN; i++) {
            vertexList[i].isVisited = false;
        }
    }


    public void topologySort(){
        int temp = curN;

        while (curN > 0){
            int delVertex = noNeigh();
            if (delVertex == -1){
                System.out.println("Cycle");
                return;
            }

            sortedArr[curN-1] = vertexList[delVertex].name;
            delVertex(delVertex);
        }

        for (int i = 0; i < temp; i++) {
            System.out.print(sortedArr[i]);
        }
    }

    private int noNeigh(){
        for (int i = 0; i < curN; i++) {
            boolean isEdge = false;
            for (int j = 0; j < curN; j++) {
                if (mas[i][j] > 0) {
                    isEdge = true;
                    break;
                }
            }

            if (!isEdge){
                return i;
            }
        }

        return -1;
    }

    private void delVertex(int index){
        for (int i = index; i < curN-1 ; i++) {
            vertexList[i] = vertexList[i+1];
        }


        for (int i = index; i < curN-1; i++) {
            delLeft(i);
        }

        for (int i = index; i < curN-1; i++) {
            delTop(i);
        }

        curN--;
    }


    private void delLeft(int row){
        for (int i = 0; i < curN; i++) {
            mas[row][i] = mas[row+1][i];
        }
    }

    private void delTop(int col){
        for (int i = 0; i < curN; i++) {
            mas[i][col] = mas[i][col+1];
        }
    }


    public void minPath(){
        int temp = 0;
        curVert = 0;

        while (temp < curN-1){
            vertexList[curVert].isVisited = true;
            temp++;

            for (int i = 0; i < curN; i++) {
                if (i == curVert){
                    continue;
                }

                if (vertexList[i].isVisited){
                    continue;
                }

                int distance = mas[curVert][i];

                if (distance == 0){
                    continue;
                }

                put(i, distance);
            }

            if (priorityQ.size()== 0){
                System.out.println("Queue empty");
            }


            Edge theEdge = priorityQ.removeMin();
            int srcVert = theEdge.scrVert;
            curVert = theEdge.destVert;

            System.out.println(vertexList[srcVert].name + " " + vertexList[curVert].name);

        }

        for (int i = 0; i < curN; i++) {
            vertexList[i].isVisited = false;
        }
    }


    private void put(int newVert, int newDist){
        int index = priorityQ.find(newVert);

        if (index != -1){
            Edge edge = priorityQ.peekN(index);
            int oldDist = edge.distance;

            if (newDist < oldDist){
                priorityQ.removeN(index);
                Edge theEdge = new Edge(curVert, newVert, newDist);
                priorityQ.insert(theEdge);
            }
        }else{
            Edge theEdge = new Edge(curVert, newVert, newDist);
            priorityQ.insert(theEdge);
        }
    }
}
