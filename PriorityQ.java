import javax.xml.stream.events.EntityDeclaration;

public class PriorityQ {
    private final int SIZE = 20;
    private Edge[] qArr;
    private int sizeCur;

    public PriorityQ(){
        qArr = new Edge[SIZE];
        sizeCur = 0;
    }

    public void insert(Edge item){
        int i;

        for (i = 0; i < sizeCur; i++) {
            if (item.distance >= qArr[i].distance)
                break;
        }

        for (int j = sizeCur-1; j >= i; j--) {
            qArr[j+1] = qArr[j];
        }

        qArr[i] = item;

        sizeCur++;
    }

    public int size(){
        return sizeCur;
    }

    public Edge removeMin(){
        return qArr[--sizeCur];
    }

    public void removeN(int index){
        for (int i = index; i < sizeCur-1; i++) {
            qArr[i] = qArr[i+1];
        }

        sizeCur--;
    }

    public Edge peekN(int index){
        return qArr[index];
    }

    public int find(int index){
        for (int i = 0; i < sizeCur; i++) {
            if (qArr[i].destVert == index){
                return i;
            }
        }
        return -1;
    }
}
