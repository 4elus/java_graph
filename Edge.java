public class Edge {
    public int scrVert;
    public int destVert;
    public int distance;

    public Edge(int srcVert, int destVert, int distance){
        this.scrVert = srcVert;
        this.destVert = destVert;
        this.distance = distance;
    }
}
